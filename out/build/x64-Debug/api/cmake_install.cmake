# Install script for directory: D:/Collection2020/ZLMediaKit/api

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "D:/Collection2020/ZLMediaKit/out/install/x64-Debug")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Debug")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/ZLMediaKit/include/mk_common.h;/ZLMediaKit/include/mk_events.h;/ZLMediaKit/include/mk_events_objects.h;/ZLMediaKit/include/mk_frame.h;/ZLMediaKit/include/mk_h264_splitter.h;/ZLMediaKit/include/mk_httpclient.h;/ZLMediaKit/include/mk_media.h;/ZLMediaKit/include/mk_mediakit.h;/ZLMediaKit/include/mk_player.h;/ZLMediaKit/include/mk_proxyplayer.h;/ZLMediaKit/include/mk_pusher.h;/ZLMediaKit/include/mk_recorder.h;/ZLMediaKit/include/mk_rtp_server.h;/ZLMediaKit/include/mk_tcp.h;/ZLMediaKit/include/mk_thread.h;/ZLMediaKit/include/mk_track.h;/ZLMediaKit/include/mk_transcode.h;/ZLMediaKit/include/mk_util.h;/ZLMediaKit/include/mk_webrtc_api.h;/ZLMediaKit/include/mk_export.h;/ZLMediaKit/include/version.h")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/ZLMediaKit/include" TYPE FILE FILES
    "D:/Collection2020/ZLMediaKit/api/include/mk_common.h"
    "D:/Collection2020/ZLMediaKit/api/include/mk_events.h"
    "D:/Collection2020/ZLMediaKit/api/include/mk_events_objects.h"
    "D:/Collection2020/ZLMediaKit/api/include/mk_frame.h"
    "D:/Collection2020/ZLMediaKit/api/include/mk_h264_splitter.h"
    "D:/Collection2020/ZLMediaKit/api/include/mk_httpclient.h"
    "D:/Collection2020/ZLMediaKit/api/include/mk_media.h"
    "D:/Collection2020/ZLMediaKit/api/include/mk_mediakit.h"
    "D:/Collection2020/ZLMediaKit/api/include/mk_player.h"
    "D:/Collection2020/ZLMediaKit/api/include/mk_proxyplayer.h"
    "D:/Collection2020/ZLMediaKit/api/include/mk_pusher.h"
    "D:/Collection2020/ZLMediaKit/api/include/mk_recorder.h"
    "D:/Collection2020/ZLMediaKit/api/include/mk_rtp_server.h"
    "D:/Collection2020/ZLMediaKit/api/include/mk_tcp.h"
    "D:/Collection2020/ZLMediaKit/api/include/mk_thread.h"
    "D:/Collection2020/ZLMediaKit/api/include/mk_track.h"
    "D:/Collection2020/ZLMediaKit/api/include/mk_transcode.h"
    "D:/Collection2020/ZLMediaKit/api/include/mk_util.h"
    "D:/Collection2020/ZLMediaKit/api/include/mk_webrtc_api.h"
    "D:/Collection2020/ZLMediaKit/out/build/x64-Debug/mk_export.h"
    "D:/Collection2020/ZLMediaKit/out/build/x64-Debug/version.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/ZLMediaKit/lib/mk_api.lib")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/ZLMediaKit/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "D:/Collection2020/ZLMediaKit/release/windows64/Debug/mk_api.lib")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("D:/Collection2020/ZLMediaKit/out/build/x64-Debug/api/tests/cmake_install.cmake")

endif()

